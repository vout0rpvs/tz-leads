import React, { Component } from 'react'
import './Leads.css';
import Stats from '../Stats/Stats'
import ScrollAnimation from 'react-animate-on-scroll';
class Leads extends Component {
    state ={
        toggleDepartment:false,
        fourty:false,
        thirty1:false,
        thirty2:false
    }
    constructor(props){
        super(props);
        this.active_ref = React.createRef();
    }
    componentDidMount(){
        this.active_ref.current.focus();
    }
    buttonSwitchEmployee = () =>{
        this.setState({toggleDepartment: false})
    }
    buttonSwitchDepartment = () =>{
        this.setState({toggleDepartment: true})
    }
    handleFortyOnMouseOver = () => {
        this.setState({fourty: true})
    }
    handleFortyOnMouseLeave = () => {
        this.setState({fourty: false})
    }
    handleThirty1OnMouseOver = () => {
        this.setState({thirty1: true})
    }
    handleThirty1OnMouseLeave = () => {
        this.setState({thirty1: false})
    }
    handleThirty2OnMouseOver = () => {
        this.setState({thirty2: true})
    }
    handleThirty2OnMouseLeave = () => {
        this.setState({thirty2: false})
    }
    render(){
        return(
            <section className="Leads">
            <div className="leadListsContainer">
                <div className="leadLists">
                    <h2>Lead Lists</h2>
                    <p>We have over 20M US B2B leads in our database with titles, e-mails, phone numbers, locations, 
                    and dozens of other relevant fields. See the breakdown of our leads by company employee size or department.</p>
                </div>
                <div className="employeeDepartment">
                    <div className="btnWrap">
                        <button 
                        ref={this.active_ref} 
                        onClick={() => this.buttonSwitchEmployee() } 
                        className={this.state.toggleDepartment ? '' : 'active'}>Employee</button>
                        <button 
                        onClick={() => this.buttonSwitchDepartment()}
                        className={!this.state.toggleDepartment ? '' : 'active'}>Department</button>
                    </div>
                    <div>
                        <div className="StatRow1">
                            <Stats>
                            { !this.state.toggleDepartment ? '40% - 1000+ Employees' : 'Sales - 40%'}
                            </Stats>
                        </div>
                        <ul className='Pie'>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='First'><div className='Slice-contents blue'></div></ScrollAnimation>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='Second'><div className='Slice-contents blue'></div></ScrollAnimation>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='Third'><div className='Slice-contents green'></div></ScrollAnimation>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='Fourth'><div className='Slice-contents green'></div></ScrollAnimation>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='Fifth'><div className='Slice-contents lightblue'></div></ScrollAnimation>
                        <ScrollAnimation key={Math.random()} className="Slice" animateIn='Sixth'><div className='Slice-contents lightblue'></div></ScrollAnimation>
                        </ul>
                        <div className="StatRow2">
                            <Stats style={{left: '-60px', position: 'absolute',top: '-10px'}}>{!this.state.toggleDepartment ? '30% - 250-1K Employees' : 'Marketing 30%'}</Stats>
                            <Stats style={{top:'20px',right:'-30px', position:'absolute'}}>{!this.state.toggleDepartment ? '30% - <250 Employees' : 'Information & Technology - 30%'}</Stats>
                        </div>
                    </div>
                </div>
            </div>
            <div className="Companies">
                <div>
                    <ScrollAnimation className="usCompanies" animateIn={"Spin"} >
                        <ScrollAnimation className="usCompaniesInside" animateIn={"Respin"} >840K + US Companies</ScrollAnimation>
                    </ScrollAnimation>
                </div>
                <div>
                    <h2>US Companies</h2>
                    <p>We have data on over 840K+ US companies with fields that include employee size, 
                        technologies, revenue, company info, industry and dozens of other relevant fields.</p>
                </div>
            </div>

            <div className="Transparency">
                <h2>Transparency</h2>
                <p>Too many lead providers are shrouded in mystery , where you pay a large sum and don’t really know what you are getting, 
                    only to be disapointed with the results. We provide full transparency on where we get our leads and the validity of 
                    those leads.</p>
            </div>
            <div className="svgWrap">
               <div className="statsWrap">
                   <div><Stats>10% Human Input</Stats></div>
                   <div className="figureWrap">
                   <ScrollAnimation className="getLeads" animateIn={"Spin"} >
                    <ScrollAnimation className="getLeadsInside" animateIn={"Respin"} >Where do we get our leads?</ScrollAnimation>
                   </ScrollAnimation>
                   </div>
                   <div style={{position:'relative'}}> 
                        <Stats>60% Chrome Extensions</Stats>
                        <Stats>30% Machine Learning</Stats>
                   </div>
               </div>
               <div className="listWrap">
                   <ul>
                       <li><div><p><span>Validated</span>&nbsp;Direct Phone Numbers</p><p><span>Validated</span>&nbsp;Emails</p></div></li>
                       <li><div><p>Different technologies used at 840k companies</p><small>Avg of 30 technologies per company</small></div></li>
                       <li><div><p><span>Validated</span>&nbsp;Consumer E-mails & Phone Numbers</p></div></li>
                   </ul>
               </div>
            </div>
        </section>
        )
    }
}

export default Leads