import React from 'react'
import './Stats.css'
const stats = props => (
    <div className="Stats" style={props.style} onMouseOver={props.mouseEnter}  onMouseLeave={props.mouseLeave}>
        <p>{props.children}</p>
    </div>
)
export default stats