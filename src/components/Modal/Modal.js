import React from 'react';
import './Modal.css'
const modal = (props) => (
    <div className="myModal modal" onClick={props.toggleHandler}>
    <div className="modal-content">
      <span className="close" onClick={props.toggleHandler}>&times;</span>

      
    </div>
  </div>
)
export default modal;
