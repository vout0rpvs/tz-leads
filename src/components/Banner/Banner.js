import React from 'react';

import './sass/Banner.sass';

class Banner extends React.Component {
    render() {
        return (
            <div className="banner_wrap prefinery-form-cta">

                Validate 10,000 E-mails Free with Purchase of Lead List! Limited Time Only
            </div>
        )
    }
}

export default Banner
