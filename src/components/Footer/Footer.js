import React from 'react';
import './Footer.css'
const footer = () => (
    <footer>
        <div>
        <p>
        One of the largest clean B2B databases in the United States. Contact us to chat about the data and your needs!
        </p>
        <button className="Contact prefinery-form-cta" type="button">
            {/* <a href="https://app.prefinery.com/a/projects/10708/edit">
                Contact
            </a> */}
            Contact
        </button>
        <nav>
            <a href="https://www.dev.twelvezeros.co/privacy">Privacy</a>
            <a href="https://www.dev.twelvezeros.co/terms">Terms</a>
        </nav>
        </div>
    </footer>
)
export default footer
