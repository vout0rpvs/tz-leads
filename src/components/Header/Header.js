
import React, { Component } from 'react'

import Spotify from '../Icons/Spotify/Spotify'

import './Header.scss'

import figureIco from './image/figure.png'
import salesforceIco from './image/salesforce.png'
import slackIco from './image/slack.png'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false
        }
    }
    openModal = () => {
        this.setState({ openModal: !this.state.openModal })
    }
    render() {
        return (
            <header>
                <section>
                    <div className="Header">
                        <div className="TZlogo">TZ Leads</div>
                        <nav>
                            <a href="javascript:void(0);" className="prefinery-form-cta">Contact</a>
                            <a href="https://drift.me/patrickkellenbeger">Get a Demo</a>
                        </nav>
                    </div>
                    <div className="HeadText">
                        <h1>Intelligent Lead Generation</h1>
                        <small>Every marketing team needs leads. Start with one of the largest validated lead sets in the world</small>
                        <p>Transparent. Validated. Leads.</p>

                    </div>
                    <div className="imageWrap"><img src={figureIco} alt="leads_figure" /></div>
                    <div className="contactWrap">
                        <button className="Contact prefinery-form-cta" type="button">Contact</button>
                    </div>
                </section>
                <div className="FewCustomers">
                    <h2>A Few Happy Customers</h2>
                    <div className="HeaderImages">
                        <div ><img src={salesforceIco} alt="" /></div>
                        <div><img src={slackIco} alt="" /></div>
                        <div>
                            <Spotify />
                        </div>

                    </div>
                </div>
            </header>
        )
    }
}
export default Header
