import React from 'react'
import './About.css'
const about = () => (
    <div className="About">
        <div>
        <div className="aboutBlock1"><img src={require('./image/Layer-3.png')} alt=""/>
            <p>Each e-mail that corresponds to a lead is run through our proprietary 
            e-mail validation software which will determine the safety to send. If the e-mail has any red flags our machine learning models will score that e-mail to ensure you have confidence in what you are sending.
            When you buy a list, the data is validated through the date of purchase.</p></div>
        </div>
        <div className="aboutBlock2">
            <div>
                <img src={require('./image/Layer-4.png')} alt=""/>
                <p>Competitive Pricing. We will match any competitor. We price based on the amount of leads and the targeting requests.</p></div>
            <div>
                <img src={require('./image/Layer-7.png')} alt=""/>
                <p>We provide lead lists in CSV or Excel formats depending on your needs. Contact us for access to our API.</p></div>
            <div>
                <img src={require('./image/Layer-5.png')} alt=""/>
                <p>We have over 40 filterable fields corresponding to each lead. We can filter by title, department, location, company size, revenue to name a few.</p></div>
            <div>
                <img src={require('./image/Layer-6.png')} alt=""/>
                <p>Live chat support to answer any questions you have before or after you purchase. Click the little box in the corner of your screen!</p></div>
        </div>
        
    </div>
)
export default about