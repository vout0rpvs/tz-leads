import React, { Component } from 'react'

import Header from '../components/Header/Header'
import Leads from '../components/Leads/Leads'
import About from '../components/About/About'
import Footer from '../components/Footer/Footer'
import Banner from '../components/Banner/Banner'

import './Layout.css';

class Layout extends Component{
     render(){
        return(
            <div className="Layout">
                <Banner />
                <Header />
                <Leads />
                <About />
                <Footer />
            </div>
        )
    }
}
export default Layout
