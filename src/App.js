import React, { Component } from 'react';
import Layout from './containers/Layout'
import './sass/App.scss';

class App extends Component {
  render() {
    return (
      <Layout />
    );
  }
}

export default App;
