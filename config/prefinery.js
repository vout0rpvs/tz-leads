var _pfy = _pfy || [];
(function () {
    function pfy_load()

    {
        var pfys = document.createElement('script');
        pfys.type = 'text/javascript';
        pfys.async = true;
        pfys.src = 'https://widget.prefinery.com/widget/v2/bvo5lj46.js';
        var pfy = document.getElementsByTagName('script')[0];
        pfy.parentNode.insertBefore(pfys, pfy);
    }
    if (window.attachEvent)

    {
        window.attachEvent('onload', pfy_load);
    } else {
        window.addEventListener('load', pfy_load, false);
    }
})();